variable "do_api_token" {}
variable "do_sshkey_id" {}

provider "digitalocean" {
    token = "${var.do_api_token}"
}

# Variables
locals {
  master_count = 1 
  worker_count = 1
}

resource "digitalocean_droplet" "masters" {
  private_networking = true
  count = "${local.master_count}"
  name = "master-${count.index}"
  image = "ubuntu-18-04-x64"
  size = "2gb"
  region = "fra1"
  ssh_keys = ["${var.do_sshkey_id}"]
}

resource "digitalocean_droplet" "workers" {
  private_networking = true
  count = "${local.worker_count}"
  name = "worker-${count.index}"
  image = "ubuntu-18-04-x64"
  size = "2gb"
  region = "fra1"
  ssh_keys = ["${var.do_sshkey_id}"]
}

## Ansible mirroring hosts section
# Using https://github.com/nbering/terraform-provider-ansible/ to be installed manually (third party provider)
# Copy binary to ~/.terraform.d/plugins/

resource "ansible_host" "masters" {
  count = "${local.master_count}"
  inventory_hostname = "master-${count.index}"
  groups = ["ansible_nodes"]
  vars = {
    ansible_host = "${element(digitalocean_droplet.masters.*.ipv4_address, count.index)}"
    k8s_apiserver_advertise_address = "${element(digitalocean_droplet.masters.*.ipv4_address_private, count.index)}"
  }
}

resource "ansible_host" "workers" {
  count = "${local.worker_count}"
  inventory_hostname = "worker-${count.index}"
  groups = ["ansible_nodes"]
  vars = {
    ansible_host = "${element(digitalocean_droplet.workers.*.ipv4_address, count.index)}"
    k8s_apiserver_advertise_address = "${element(digitalocean_droplet.workers.*.ipv4_address_private, count.index)}"
  }
}

resource "ansible_group" "all" {
  inventory_group_name = "all"
  vars = {
    ansible_python_interpreter = "/usr/bin/python3"
  }
}
