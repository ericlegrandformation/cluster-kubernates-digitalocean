# Cluster KUBERNATES - DigitalOcean

![a](./images/LogoKubernates.png?zoomresize=480%2C240)

Ce projet va vous permettre de :
 * d'utiliser Terraform pour provisionner l'infrastructure dans DigitalOcean
 * d'utiliser Ansible pour installer et configurer KUBERNATES


## Pré-requis

Ce qui est requis pour commencer avec ce projet...

- Un ordinateur connecté à internet préférablement avec Linux
- des connaissances de base en système Linux, réseaux informatique et applications Web
- des notions de programmation (scripting, Python, PHP, ..)
- avoir déjà utilisé Git, VirtualBox et un outil de Dev (par exemple VSCode) seront un plus utile

La création et configuration d'un compte **Digital Ocean**

- Rendez-vous à l'adresse ci dessous et utiliser le code Promo: ACTIVATE10 pour obtenir 10$ de crédit
[https://cloud.digitalocean.com/registrations/new?onboarding_origin=kubernetes](https://cloud.digitalocean.com/registrations/new?onboarding_origin=kubernetes)

- créer un nouveau projet appelé "ClusterKubernates" par exemple

- avoir réalisé la configuration de votre compte Digital Ocean, notamment,
créer le **Token** API Digital Ocean (menu API, click sur "Generate New Token") et la **clé ssh** pour accéder à Digital Ocean Cloud sans mot de passe (menu Security, click sur "add SSH Key" ): [https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean](https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean)


Procéder à l'installation de **Terraform**:

* Terraform n'est pas disponible sous forme de dépôt ubuntu/debian. Pour l'installer il faut le télécharger et l'installer manuellement:
```
$ cp /tmp
$ wget https://releases.hashicorp.com/terraform/0.12.6/terraform_0.12.6_linux_amd64.zip
$ sudo unzip ./terraform_0.12.6_linux_amd64.zip -d /usr/local/bin/
```
* puis tester l'installation:
```
$ terraform --version
```

Nous aurons aussi besoin d'un outil supplémentaire pour pouvoir faire un **inventaire dynamique**:

* suivez les instructions d'installation : [https://github.com/nbering/terraform-provider-ansible/](https://github.com/nbering/terraform-provider-ansible/)
 

Téléchargez également ce script qui servira effectivement d'inventaire:
```
$ wget https://raw.githubusercontent.com/nbering/terraform-inventory/master/terraform.py
```

## Installation

Nous allons installer un cluster KUBERNATES à l'aide de Terraform et Ansible chez DigitalOcean.

Ce projet reprend ce qui a été mis en place avec notre projet https://framagit.org/ericlegrandformation/cluster-kubernates-stand-alone.git pour installer et configurer Kubernates et l'adapte pour un provisionnement CLOUD, ici avec DigitalOcean.  

C'est parti !

Sur votre machine hôte, depuis votre répertoire **/home/'votre nom utilisateur'**, 

* dans un terminal, taper :
``` 
$ git clone https://framagit.org/ericlegrandformation/cluster-kubernates-digitalocean.git 
```

* ouvrer le dossier "cluster-kubernates-digitalocean" **avec VSCode**

* ouvrer un **nouveau terminal** dans VSCode

* créer un environnement virtuel python avec la commande
 ```
$ virtualenv --python=python3.5 venv
 ```

* activer l'environnement virtuel python avec la commande
 ```
$ source ./venv/bin/activate
  ```

* installer quelques pré-requis avec la commande
 ```
$ sudo apt install libffi-dev python-dev libssl-dev
  ```

* installer ansible avec la commande
 ```
$ pip install ansible
  ```

* vérifier la version ansible avec
 ```
$ ansible --version (version 2.8 mini)
  ```

Enregistrement du token et de la clé ssh dans le fichier terraform.tfvars:

* copiez le fichier  `terraform.tfvars.dist`  et renommez le en enlevant le  `.dist`

* rendez vous sur votre compte Digital Ocean pour récupérer le **token** et copiez le hash du token

* collez le token dans le fichier de variables  `terraform.tfvars` entre les guillemets de la ligne: do_api_token  = "ajouter_ici_token "

* récupérer la signature de votre **clé ssh** :
```
$ cd /home/"utilisateur"/.ssh
$ ssh-keygen -E md5 -lf ~/.ssh/id_rsa.pub | awk '{print $2}'
```

* ajoutez le code retourné par la commande ci dessus (sans le MD5:), entre les guillemets de la ligne: do_sshkey_id  =  "ajouter_ici_sshkey "

Procéder à la création des serveurs:

- Lancer la commande :
```
$ cd /home/"utilisateur"/cluster-kubernates-digitalocean
$ terraform init
$ terraform apply
```

- Testez l'inventaire dynamique : 
```
$ chmod +x terraform.py && ./terraform.py
```

Vous devriez avoir du texte JSON en retour de ce programme.


* Configurer les VM master-1 et worker-1 avec la commande
 ```
$ ansible-playbook roles/main.yml
  ```

* Vérifier l'installation du master et du worker Kubernates avec la commande
 ```
$ ssh vagrant@192.168.50.11
  taper le mot de passe "vagrant" et une fois connecté:
$ kubectl get nodes
  si tout va bien, lire:
NAME       STATUS   ROLES    AGE     VERSION
master-1   Ready    master   5d10h   v1.16.2
worker-1   Ready    <none>   5d10h   v1.16.2 
  ```
  
*Bravo, vous venez d'installer KUBERNATES dans le CLOUD avec Terraform et Ansible.*

***

## Versions

**Dernière version stable :** 0.1
**Dernière version :** 0.1
Liste des versions : [Cliquer pour afficher](https://framagit.org/ericlegrandformation/cluster-kubernates-digitalocean.git)

## Auteurs

* **Eric Legrand** _alias_ [@ericlegrand](https://framagit.org/ericlegrandformation/cluster-kubernates-digitalocean.git)